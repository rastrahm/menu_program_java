package menu.util;

import java.sql.ResultSet;
import java.sql.SQLException;

import menu.data.db;
import menu.security.SafeSQL;

public class Table {
	public static String getTable(String table) {
		db conn = new db();
		String resp = "";
		try {
			ResultSet tables = conn.Select(menu.security.SafeSQL.getTable(table));
			int i = 0;
			while (tables.next()) {
				if (i > 0) {
					resp += ",";
				}
				resp += "{";
				resp += "\"str_column\" : \"" + tables.getString("str_column") + "\",";
				resp += "\"str_type\" : \"" + tables.getString("str_type") + "\",";
				resp += "\"int_length\" : \"" + tables.getString("int_length") + "\"";
				resp += "}";
				i++;
			}
		} catch (SQLException e) {
			resp = e.toString();
		}
		return resp;
	}
}
