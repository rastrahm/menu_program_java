package menu.util;

import java.sql.ResultSet;
import java.sql.SQLException;

import menu.data.db;
import menu.security.SafeSQL;

public class Dictionary {
	public static String getDictionary() {
		db conn = new db();
		String resp = "";
		try {
			ResultSet dict = conn.Select(menu.security.SafeSQL.getDictionary());
			resp += "\"dictionary\":[";
			int i = 0;
			while (dict.next()) {
				if (i > 0) {
					resp += ",";
				}
				resp += "{";
				resp += "\"str_name\" : \"" + dict.getString("str_name") + "\",";
				resp += "\"str_value\" : \"" + dict.getString("str_value") + "\"";
				resp += "}";
				i++;
			}
			resp += "]";
		} catch (SQLException e) {
			resp = e.toString();
		}
		return resp;
	}
}
