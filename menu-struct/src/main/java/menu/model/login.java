package menu.model;
/**
*
* @author Rolando Strahm
*
* Objeto del usuario para el login
*
*/
public class login {
	public String user;

	public String pass;

	public login() {
	}

	public login(String user, String pass) {
		super();
		this.user = user;
		this.pass = pass;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}
}