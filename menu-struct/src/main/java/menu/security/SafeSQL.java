package menu.security;

import java.util.Map;

public class SafeSQL {
	//Verify the access user whit their user name and their password
	public static String getSelect(Map<String, String> data) {
		String query = "SELECT * FROM public.fnc_user_pass('" + data.get("user") + "', public.encrypt('" + data.get("pass") + "', 'prueba', 'bf'))";
		return query;
	}

	//Verify the consistency of JWT object
	public static String getVerify(Map<String, String> data) {
		String query = "SELECT * FROM public.fnc_user_verify('" + data.get("id") + "', '" + data.get("user") + "')";
		return query;
	}

	//Get a key from parameter table
	public static String getKey(String key) {
		String query = "SELECT * FROM public.fnc_get_parameter('" + key + "')";
		return query;
	}

	//Return the query for the object name form menu_object table
	public static String getObject(String profile, String action) {
		String query = "";
		if (action.equals("menu")) {
			query = "SELECT * FROM public.fnc_previews_get_list()";
		} else {
			query = "SELECT * FROM public.fnc_men_obj(" + profile + ", '" + action + "')";
		}
		return query;
	}

	//Return the SQL for menu
	public static String getMenu(String profile) {
		String query = "SELECT * FROM public.fnc_men(" + profile + ")";
		return query;
	}

	//Return the SQL sentence for the sub menu
	public static String getSubmenu(String profile, String menu) {
		String query = "SELECT * FROM public.fnc_men_sub(" + profile + ", " + menu + ")";
		return query;
	}

	//Return the Dictionary of aplication
	public static String getDictionary() {
		return "SELECT * FROM public.fnc_dictionary_get_all()";
	}

	//Return the structure of the mentioned table
	public static String getTable(String table) {
		return "SELECT * FROM public.fnc_table_descript('" + table + "')";
	}
}
