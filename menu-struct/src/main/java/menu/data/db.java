package menu.data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;

import menu.util.Params;

public class db {
	public Connection connect() {
		Connection conn = null;
		Params parm = new Params();
		String strData = parm.getParam("dns", "data.properties");
		String strHost = parm.getParam("host", "data.properties");
		String strName = parm.getParam("dbname", "data.properties");
		String strUser = parm.getParam("user", "data.properties");
		String strPass = parm.getParam("pass", "data.properties");
		String strDataBase = "jdbc:" + strData + ":" + strHost + "/" + strName;
		try {
			Class.forName("org." + strData + ".Driver");
			try {
				conn = DriverManager.getConnection(strDataBase, strUser, strPass);
			} catch (SQLException ex) {
				while (ex != null) {
	                System.out.println ("SQLState: " + ex.getSQLState ());
	                System.out.println ("Message:  " + ex.getMessage ());
	                System.out.println ("Vendor:   " + ex.getErrorCode ());
	                ex = ex.getNextException ();
	                System.out.println ("");
	            }
			}
			return conn;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return null;
	}

	public ResultSet Select(String query) {
		Connection conn = connect();
		try {
			Statement stmt = conn.createStatement();
			ResultSet response = stmt.executeQuery(query);
			return response;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public void exitConnect(Connection conn) {
		try {
			conn.close();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	}
}
