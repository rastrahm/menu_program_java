##
# You should look at the following URL's in order to grasp a solid understanding
# of Nginx configuration files in order to fully unleash the power of Nginx.
# https://www.nginx.com/resources/wiki/start/
# https://www.nginx.com/resources/wiki/start/topics/tutorials/config_pitfalls/
# https://wiki.debian.org/Nginx/DirectoryStructure
#
# In most cases, administrators will remove this file from sites-enabled/ and
# leave it as reference inside of sites-available where it will continue to be
# updated by the nginx packaging team.
#
# This file will automatically load configuration files provided by other
# applications, such as Drupal or Wordpress. These applications will be made
# available underneath a path with that package name, such as /drupal8.
#
# Please see /usr/share/doc/nginx-doc/examples/ for more detailed examples.
##

# Default server configuration
#
server {
	listen 8000;
	listen [::]:8000;

	# Add index.php to the list if you are using PHP
	index index.php index.html index.htm index.nginx-debian.html;

	server_name htmlphp.local;

	location / {
        proxy_bind $server_addr;
	#	try_files $uri $uri/ =404;
		autoindex on;
        root /home/rolando/Programacion/www/menu_program/FrontEnd/HTML;
        proxy_pass http://htmlphp.local:8000;
	}

	#location /users/ {
    #    proxy_bind $server_addr;
	#	try_files $uri $uri/ =404;
	#	autoindex on;
    #    root /home/rolando/Programacion/www/menu_program/BackEnd/PHP;
    #    proxy_pass http://127.0.0.1:8000/users;
	#}

    #location /functions/ {
    #    proxy_bind $server_addr;
	#	try_files $uri $uri/ =404;
	#	autoindex on;
    #    root /home/rolando/Programacion/www/menu_program/BackEnd/PHP;
    #    proxy_pass http://127.0.0.1:8000/functions;
	#}

    #location /outFunctions/ {
    #    proxy_bind $server_addr;
	#	try_files $uri $uri/ =404;
	#	autoindex on;
    #    root /home/rolando/Programacion/www/menu_program/BackEnd/PHP;
    #    proxy_pass http://127.0.0.1:8000/outFunctions;
	#}

    #location /struct/ {
    #    proxy_bind $server_addr;
	#	try_files $uri $uri/ =404;
	#	autoindex on;
    #    root /home/rolando/Programacion/www/menu_program/BackEnd/PHP;
    #    proxy_pass http://127.0.0.1:8000/struct;
	#}

	location ~ \.php$ {
		include snippets/fastcgi-php.conf;
		fastcgi_pass unix:/var/run/php/php7.2-fpm.sock;
	}

	# deny access to .htaccess files, if Apache's document root
	# concurs with nginx's one
	#
	#location ~ /\.ht {
	#	deny all;
	#}
}
