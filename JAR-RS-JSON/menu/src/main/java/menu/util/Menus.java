package menu.util;

import java.sql.ResultSet;
import java.sql.SQLException;

import menu.data.db;
import menu.security.SafeSQL;

public class Menus {
	public static String getMenus(String strProfile) {
		db conn = new db();
		String resp = "";
		try {
			ResultSet menus = conn.Select(menu.security.SafeSQL.getMenu(strProfile));
			ResultSet submenus = null;
			resp += "\"menu\": { \"menus\": [";
			int i = 0;
			int j = 0;
			while (menus.next()) {
				if (i > 0) {
					resp += ",";
				}
				resp += "{";
				resp += "\"big_id\" : \"" + menus.getString("big_id") + "\",";
				resp += "\"str_menu\" : \"" + menus.getString("str_menu") + "\",";
				resp += "\"txt_description\" : \"" + menus.getString("txt_description") + "\",";
				submenus = conn.Select(menu.security.SafeSQL.getSubmenu(strProfile, menus.getString("big_id")));
				resp += "\"str_submenu\" : [";
				while (submenus.next()) {
					if (j > 0) {
						resp += ",";
					}
					resp += "{";
					resp += "\"big_id\" : \"" + submenus.getString("big_id") + "\",";
					resp += "\"big_men_id\" : \"" + submenus.getString("big_men_id") + "\",";
					resp += "\"str_menu\" : \"" + submenus.getString("str_menu") + "\",";
					resp += "\"txt_description\" : \"" + submenus.getString("txt_description") + "\",";
					resp += "\"str_action\" : \"" + submenus.getString("str_action") + "\",";
					resp += "\"str_shortcut\" : \"" + submenus.getString("str_shortcut") + "\"";
					resp += "}";
					j++;
				}
				resp += "]";
				j = 0;
				resp += "}";
				i++;
			}
			resp += "]}";
		} catch (SQLException e) {
			resp = e.toString();
		}
		return resp;
	}
}
