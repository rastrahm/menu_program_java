package menu;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import menu.model.login;
import menu.data.db;
import menu.data.StrSQL;
import menu.security.Jwt;
import menu.security.SafeSQL;
import menu.util.Menus;
import menu.util.ArrayJson;

@Path("struct/{table}")
public class RestStruct {

	//El metodo GET de struct debe devolver la estructura de la tabla para el Nuevo Registro
	@GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response doStruct(@Context HttpHeaders headers, @PathParam("table") String table){
		MultivaluedMap<String, String> headerParams = headers.getRequestHeaders();
		Map<String, Cookie> pathParams = headers.getCookies();
        String jwtToken = headerParams.getFirst("jwt");
        String json = "";
        if (!(jwtToken == null)) {
        	db conn = new db();
        	Map<String, String> mapToken = new ConcurrentHashMap<>();
        	mapToken = menu.security.Jwt.parseJWT(jwtToken);
        	if (!(mapToken == null)) {
        		ResultSet safe = conn.Select(menu.security.SafeSQL.getVerify(mapToken));
        		try {
        			safe.next();
					if (safe.getString("result").equals("t")) {
						json = "{";
						json += "\"jwt\": \"" + menu.security.Jwt.getJWT(mapToken.get("id"), mapToken.get("user"), mapToken.get("profile"), 3500000) + "\",";
						json += "\"table\": \"" + table + "\",";
						json += "\"data\": [" + menu.util.Table.getTable(table) + "]";
						json += "}";
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					json = "{\"message\": \"Falla en el servicio: " + e.toString() + "\"}";
				}
        	} else {
        		json = "{\"\": \"\"}";
        	}
        } else {
        	json = "{\"\": \"\"}";
        }
		return Response.ok(json,MediaType.APPLICATION_JSON).build();
    }
}
