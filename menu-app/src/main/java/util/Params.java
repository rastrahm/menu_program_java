package menu.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.Properties;

public class Params {
	public String getParam(String stgParam, String strFile) {
		Properties prp = new Properties();
		try {
			prp.load(new FileReader(getFile(strFile)));
			return prp.getProperty(stgParam);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	private String getFile(String strFile) {
		URL urlDire = getClass().getResource("ruta.txt");
		String strDire = urlDire.toString();
		String[] arrDire = strDire.split("/");
		arrDire[0] = arrDire[0].substring(5, arrDire[0].length());
		for (int i = 0; i < arrDire.length; i++) {
			if (i == 0) {
				strDire = arrDire[i];
			} else {
				if (!arrDire[i].equals("util")) {
					strDire += "/" + arrDire[i];
				} else {
					strDire += "/config";
					break;
				}
			}
		}
		strFile = strDire + "/" + strFile;
		return strFile;
	}
}
