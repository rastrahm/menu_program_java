package menu;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import com.sun.jersey.json.impl.provider.entity.JSONJAXBElementProvider$App;
import com.sun.jersey.json.impl.provider.entity.JSONArrayProvider$App;
import com.sun.jersey.json.impl.provider.entity.JSONObjectProvider$App;
import com.sun.jersey.json.impl.provider.entity.JSONRootElementProvider$App;
import com.sun.jersey.json.impl.provider.entity.JSONListElementProvider$App;

import menu.model.login;
import menu.data.db;
import menu.data.StrSQL;
import menu.security.Jwt;
import menu.security.SafeSQL;
import menu.util.Menus;
import menu.util.ArrayJson;

/**
 *
 * @author rolando
 * Tengo que crear una superclase que arranque de application para declararla en la web.xml
 */
@Path("/")
public class RestApplication {

	//El metódo GET de la ruta root debe retornar, en caso de que exista el JWT en el HEAD la estructura del menú, de lo contrario un array en blanco
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response doGet(@Context HttpHeaders headers) {
		MultivaluedMap<String, String> headerParams = headers.getRequestHeaders();
		Map<String, Cookie> pathParams = headers.getCookies();
        String jwtToken = headerParams.getFirst("jwt");
        String json = "";
        if (!(jwtToken == null)) {
        	db conn = new db();
        	Map<String, String> mapToken = new ConcurrentHashMap<>();
        	mapToken = menu.security.Jwt.parseJWT(jwtToken);
        	if (!(mapToken == null)) {
        		ResultSet safe = conn.Select(menu.security.SafeSQL.getVerify(mapToken));
        		try {
        			safe.next();
					if (safe.getString("result").equals("t")) {
						json = "{";
						json += "\"data\": [{\"big_id\": \"" + mapToken.get("id") + "\", \"big_profile\": \"" + mapToken.get("profile") + "\"}],";
						json += "\"jwt\": \"" + menu.security.Jwt.getJWT(mapToken.get("id"), mapToken.get("user"), mapToken.get("profile"), 3500000) + "\",";
						json += menu.util.Menus.getMenus(mapToken.get("profile"));
						json += "}";
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					json = "{\"message\": \"Falla en el servicio: " + e.toString() + "\"}";
				}
        	} else {
        		json = "{\"\": \"\"}";
        	}
        } else {
        	json = "{\"\": \"\"}";
        }
		return Response.ok(json,MediaType.APPLICATION_JSON).build();
	}

	//El metodo POST en caso de que tenga los campos user y pass (recordar que el pass esta encriptado por public.encrypt(pass, prueba, bf)
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
    public Response doPost(login login) {
		String json = "";
		db conn = new db();
		Map<String, String> params = new ConcurrentHashMap<>();
		params.put("user", login.getUser());
		params.put("pass", login.getPass());
		try {
			ResultSet data = conn.Select(menu.security.SafeSQL.getSelect(params));
			if (!(data == null)) {
				data.next();
				json = "{";
				json += "\"data\": [{\"big_id\": \"" + data.getString("big_id") + "\", \"big_profile\": \"" + data.getString("big_profile") + "\"}],";
				json += "\"jwt\": \"" + menu.security.Jwt.getJWT(data.getString("big_id"), login.getUser(), data.getString("big_profile"), 3500000) + "\",";
				json += menu.util.Menus.getMenus(data.getString("big_profile")) + ",";
				json += menu.util.Dictionary.getDictionary();
				json += "}";
			} else {
				json = "{\"message\": \"Usuario invalido\"}";
			}
		} catch (Exception e) {
			json = "{\"message\": \"Falla en el servicio: " + e.toString() + "\"}";
		}
		return Response.ok(json,MediaType.APPLICATION_JSON).build();
    }

}
