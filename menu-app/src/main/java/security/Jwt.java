package menu.security;

import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.security.Key;
import io.jsonwebtoken.*;
import io.jsonwebtoken.security.SignatureException;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.xml.bind.DatatypeConverter;

import menu.data.*;
import menu.security.SafeSQL;

public class Jwt {
	public static String getJWT(String id, String subject, String issuer, long ttlMillis) {

		//Set the parameters for security
		db conn = new menu.data.db();

	    //The JWT signature algorithm we will be using to sign the token
	    SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;

	    long nowMillis = System.currentTimeMillis();
	    Date now = new Date(nowMillis);

	    //We will sign our JWT with our ApiKey secret
	    ResultSet secretKey = conn.Select(menu.security.SafeSQL.getKey("JWT_KEY"));
	    byte[] apiKeySecretBytes;
		try {
			//Let's set the JWT Claims
			secretKey.next();
			apiKeySecretBytes = DatatypeConverter.parseBase64Binary(secretKey.getString("valor"));
			Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());
			JwtBuilder builder = Jwts.builder().setId(id)
												.setIssuedAt(now)
												.setSubject(subject)
												.setIssuer(issuer)
												.signWith(signatureAlgorithm, signingKey);

			//if it has been specified, let's add the expiration
			if (ttlMillis >= 0) {
				long expMillis = nowMillis + ttlMillis;
				Date exp = new Date(expMillis);
				builder.setExpiration(exp);
			}

			//Builds the JWT and serializes it to a compact, URL-safe string
			return builder.compact();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public static Map<String, String> parseJWT(String jwt) {
		//Set the parameters for security
		db conn = new menu.data.db();
		ResultSet secretKey = conn.Select(menu.security.SafeSQL.getKey("JWT_KEY"));

		//This line will throw an exception if it is not a signed JWS (as expected)
	    Claims claims;
		try {
			secretKey.next();
			claims = Jwts.parser()
						   .setSigningKey(DatatypeConverter.parseBase64Binary(secretKey.getString("valor")))
						   .parseClaimsJws(jwt).getBody();
			Map<String, String> response = new ConcurrentHashMap<>();
			System.out.println("ID: " + claims.getId());
			response.put("id", claims.getId());
			System.out.println("Subject: " + claims.getSubject());
			response.put("user", claims.getSubject());
			System.out.println("Issuer: " + claims.getIssuer());
			response.put("profile", claims.getIssuer());
			System.out.println("Expiration: " + claims.getExpiration());
			return response;
		} catch (SignatureException | ExpiredJwtException | UnsupportedJwtException | MalformedJwtException
				| IllegalArgumentException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
