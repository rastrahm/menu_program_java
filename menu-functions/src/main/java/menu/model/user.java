package menu.model;

public class user {
	public Integer big_id;

	public String menu;

	public String str_name;

	public String str_pro_name;

	public int bol_active;

	public user() {
	}

	public user(Integer big_id, String menu, String str_name, String str_pro_name, int bol_active) {
		super();
		this.big_id = big_id;
		this.menu = menu;
		this.str_name = str_name;
		this.str_pro_name = str_pro_name;
		this.bol_active = bol_active;
	}

	public Integer getBig_id() {
		return big_id;
	}

	public void setBig_id(Integer big_id) {
		this.big_id = big_id;
	}

	public String getMenu() {
		return menu;
	}

	public void setMenu(String menu) {
		this.menu = menu;
	}

	public String getStr_name() {
		return str_name;
	}

	public void setStr_name(String str_name) {
		this.str_name = str_name;
	}

	public String getStr_pro_name() {
		return str_pro_name;
	}

	public void setStr_pro_name(String str_pro_name) {
		this.str_pro_name = str_pro_name;
	}

	public int getBol_active() {
		return bol_active;
	}

	public void setBol_active(int bol_active) {
		this.bol_active = bol_active;
	}

}
