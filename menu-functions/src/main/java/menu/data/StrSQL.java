package menu.data;

import java.util.Map;

import menu.util.Verify;

/**
*
* @author Rolando Strahm
*
* Object with generate a SQL string
* @params:
* 		method: metódo solicitado: POST, GET, PUT, DELETE
* 		target: Tabla afectada
* 		fields: Array que contiene los campos a actualizar, lo retorna de menu_options
* 		params: Array de valores a actualizar
* 		name:   Contiene en caso de GET o DELETE el id a ejecutar
*/

public class StrSQL {
	public String construct(String method, String target, String fields, Map<String, String> params, String name) {
		String strFunction = "";
		if (method.equals("GET") && (name.equals(params.get("menu")))) {
			strFunction = "SELECT * FROM public." + target + "_" + method + "_ALL(";
		} else {
			strFunction = "SELECT * FROM public." + target + "_" + method + "(";
		}
		int i = 0;
		if ((!name.isEmpty()) && ((params.isEmpty()) || (params.size() == 1)) ) {
			if (Verify.isNumeric(name)) {
				strFunction += name;
				i++;
			} else {
				strFunction += "'" + name + "'";
				i++;
			}
		}
		if (method.equals("PUT")) {
			for (Map.Entry<String, String> entry : params.entrySet()) {
				if (entry.getKey().equals("big_id")) {
					strFunction += entry.getValue();
					i++;
				}
			}
		} else if (method.equals("DELETE")) {
			strFunction += params.get("delete");
		}
		if (!params.isEmpty()) {
			String strSeparator = "";
			String[] arrFields = fields.replace("'", "").replace("{", "").replace("}", "").split(",");
			for (int h = 0; h < arrFields.length; h++) {
				if (i > 0) {
					strSeparator = ",";
				}
				for (Map.Entry<String, String> entry : params.entrySet()) {
					if (arrFields.equals(entry.getKey())) {
						strFunction += strSeparator + "'" + entry.getValue().replaceAll("&gt;", ">").replaceAll("&lt;", "<") + "'";
					}
				}
				i++;
			}
		}
		strFunction += ")";
		return strFunction;
	}
}
