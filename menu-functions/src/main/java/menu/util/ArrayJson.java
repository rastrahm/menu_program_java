package menu.util;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

public class ArrayJson {
	public static String getJson(ResultSet data) {
		String json = "";
		try {
			ResultSetMetaData columns = data.getMetaData();
			int i = 0;
			while (data.next()) {
				if (i > 0) {
					json += ",";
				}
				json += "{";
				for (int j = 0; j < columns.getColumnCount(); j++) {
					if (j > 0) {
						json += ",";
					}
					json += "\"" + columns.getColumnName(j) + "\" : \"" + data.getString(columns.getColumnName(j)) + "\"";
				}
				json += "}";
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			json = "{\"message\" : \"Error data: " + e.toString() + "\"}";
		}

		return json;
	}

}
